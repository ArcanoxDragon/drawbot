This bot is for use with Mast3rPlan's Online Whiteboard, found at http://doodle.mast3rplan.me/ .
Most features should be fairly self-explanatory. The font.txt file comes with plenty of characters
but if you want to add definitions for more (ASCII only!) you can add them. The format follows.

-- font.txt FORMAT --
Any line starting with "`" is a comment and will be ignored.
Other lines must start with an ASCII character followed by FONTWIDTH * FONTHEIGHT bits, representing
pixels in the character from left-to-right and then top-to-bottom. By default, FONTWIDTH is 5 and
FONTHEIGHT is 7. You can use any character other than "0" or "1" as a separator to help organize
rows in the character definition. 1 means that bit is on (pixel lit) and 0 means it is off
(pixel dark). If there are fewer bits than the required number for the font size, the rest will be
assumed to be "0" or off. If there are more bits than required, the extras will simply be ignored.

-- LICENSE --
You may not sell or otherwise profit off of this code in any way at all. You must retain this file
with any redistribution of the code and must properly attribute it to me (Briman) if using it.