/*
 * ASCIIConstants.h
 *
 *  Created on: May 28, 2013
 *      Author: Brian
 */

#ifndef ASCIICONSTANTS_H_
#define ASCIICONSTANTS_H_

#define FONTFILE "font.txt"
#define FONTWIDTH 5
#define FONTHEIGHT 7

bool* getLetter(char c);
void loadFont();

#endif /* ASCIICONSTANTS_H_ */
