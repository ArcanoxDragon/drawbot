#ifndef _DRAWTOOLS_H
#define _DRAWTOOLS_H

#include <string>
using std::string;

void drawPoint(int x, int y);
void drawLine(int sX, int sY, int eX, int eY);
void drawCircle(int x, int y, int r);
void drawRectangle(int x, int y, int w, int h);
void drawCharacter(int x, int y, char c);
void drawImage(int x, int y, int *image, unsigned w, unsigned h);
void drawImage(int x, int y, const char* fname);
void drawImageFast(int x, int y, const char* fname);
void compressImage(int* img, unsigned w, unsigned h);
void setColor(int r, int g, int b);
void setBrushWidth(int width);
void setFastImage(bool fastImage);
bool getFastImage();
int getBrushWidth();
void mouseDown();
void mouseUp();
void mouseMove(int x, int y);
bool isKeyDown(int key);
void RGB2HSV(double r, double g, double b, double &h, double &s, double &v);

enum ImageOverrideType
{
	OVERRIDE_NONE,
	OVERRIDE_FAST,
	OVERRIDE_SEQU
};

#endif // _DRAWTOOLS_H
