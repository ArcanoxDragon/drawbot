/*
 * imageloader.cpp
 *
 *  Created on: May 29, 2013
 *      Author: Brian
 */

#include "imageloader.h"
#include "lodepng.h"
#include <string>
#include <vector>
#include <iostream>
using namespace std;

int* loadImage(string filename, unsigned &w, unsigned &h)
{
	std::vector<unsigned char> image;
	unsigned error = lodepng::decode(image, w, h, filename);

	if (error)
	{
		cout << "decoder error " << error << ": " << lodepng_error_text(error) << endl;
		return 0;
	}

	int* ret = new int[w * h];

	for (unsigned y = 0; y < h; y += 1)
	{
		for (unsigned x = 0; x < w; x += 1)
		{
			//get RGBA components
			unsigned r = image[4 * y * w + 4 * x + 0]; //red
			unsigned g = image[4 * y * w + 4 * x + 1]; //green
			unsigned b = image[4 * y * w + 4 * x + 2]; //blue
			unsigned a = image[4 * y * w + 4 * x + 3]; //alpha

			int t = 0;
			t += r;
			t += ((g & 0xFF) << 8);
			t += ((b & 0xFF) << 16);
			t += ((a & 0xFF) << 24);
			ret[y * w + x] = t;
		}
	}

	return ret;
}
