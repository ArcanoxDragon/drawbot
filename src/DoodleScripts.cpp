/*
 * DoodleScripts.cpp
 *
 *  Created on: May 28, 2013
 *      Author: Brian
 */
#include <iostream>
#include <windows.h>
#include <string>
#include <cmath>
#include <math.h>
#include <algorithm>
#include <vector>
#include "DrawTools.h"
#include "FontLoader.h"
#include "settings.h"
using namespace std;

int main(int argc, char* argv[])
{
	loadFont(); // load the font for the text functions
	bool running = true;
	string input;
	int s1, s2, s3;
	string c1;
	POINT p;
	while (running)
	{
		cout << "What would you like to do?\n";
		cout << "\t 1. Draw Square\n";
		cout << "\t 2. Draw Circle\n";
		cout << "\t 3. Draw Text\n";
		cout << "\t   3b. Draw Advanced Text\n";
		cout << "\t 4. Draw Matrix\n";
		cout << "\t 5. Draw Image\n";
		cout << "\t   5b. Draw Image (fast)\n";
		cout << "\t 6. Clear Screen\n";
		cout << "\t   6b. Clear Part of Screen\n";
		cout << "\t 7. Set Cursor Position\n";
		cout << "\t 8. Set Color\n";
		cout << "\t 9. Set Brush Width\n";
		cout << "\t10. Reload Font\n";
		cout << "> ";
		getline(cin, input, '\n');
		if (input == "1") // Rectangle
		{
			cout << "Width: ";
			cin >> s1;
			cout << "Height: ";
			cin >> s2;
			cout << "Drawing rectangle at current mouse position in 2.5 seconds...\n";
			Sleep(2500);
			GetCursorPos(&p);
			drawRectangle(p.x, p.y, s1, s2);
		}
		else if (input == "2") // Circle
		{
			cout << "Radius: ";
			cin >> s1;
			cout << "Drawing circle at current mouse position in 2.5 seconds...\n";
			Sleep(2500);
			GetCursorPos(&p);
			drawCircle(p.x, p.y, s1);
			mouseMove(p.x, p.y);
		}
		else if (input == "3") // Text
		{
			cout << "Text: ";
			getline(cin, c1, '\n');
			bool inverted = false;
			if (c1.length() > 0)
			{
				if (c1[0] == '`')
					inverted = true;
			}
			cout << "Drawing text at current mouse position in 2.5 seconds...\n";
			Sleep(2500);
			GetCursorPos(&p);
			int px = p.x;
			int py = p.y;
			int cWid = (FONTWIDTH + 2) * getBrushWidth();
			int sWid = MAXX - p.x;
			int wLen = sWid / cWid;
			int curWid = c1.size();
			int lastSpace = 0;
			unsigned tries = 0; // prevent infinite loop if we don't find a space
			do
			{
				bool found = false;
				for (unsigned i = lastSpace + wLen; i < c1.size() && !found; i--)
				{
					if (c1[i] == ' ' || c1[i] == '\\') // we check for a manual break here to set width correctly
					{
						c1[i] = '\\';
						found = true;
						curWid -= i - lastSpace;
						lastSpace = i;
					}
				}
				tries++;
			}
			while (curWid > wLen && tries < c1.size() / wLen + 1);
			for (unsigned int i = inverted ? 1 : 0; i < c1.length(); i++)
			{
				if (c1[i] == '\\')
				{
					px = p.x;
					py += getBrushWidth() * (FONTHEIGHT + 2);
				}
				else
				{
					drawCharacter(px, py, c1[i]);
					px += getBrushWidth() * (FONTWIDTH + 2);
				}
			}
			mouseMove(p.x, p.y);
		}
		else if (input == "3b")
		{
			cout << "Text: ";
			getline(cin, c1, '\n');
			cout << "R (0 - 255): ";
			cin >> s1;
			cout << "G (0 - 255): ";
			cin >> s2;
			cout << "B (0 - 255): ";
			cin >> s3;
			int tR, tG, tB;
			int bR, bG, bB;
			tR = min(max(0, s1), 255);
			tG = min(max(0, s2), 255);
			tB = min(max(0, s3), 255);
			bR = tR / 8;
			bG = tG / 8;
			bB = tB / 8;
			char* tText = new char[c1.length()];
			bool inverted = false;
			if (c1.length() > 0)
			{
				if (c1[0] == '`')
					inverted = true;
			}
			for (unsigned int i = inverted ? 1 : 0; i < c1.length(); i++)
			{
				if (c1[i] == '\\')
				{
					tText[i] = '\\';
				}
				else
				{
					tText[i] = '~';
				}
			}
			cout << "Drawing text at current mouse position in 2.5 seconds...\n";
			cout << "Please have the color window already visible!\n";
			Sleep(2500);
			GetCursorPos(&p);
			int px = p.x;
			int py = p.y;
			int cWid = (FONTWIDTH + 2) * getBrushWidth();
			int sWid = MAXX - p.x;
			int wLen = sWid / cWid;
			int curWid = c1.size();
			int lastSpace = 0;
			unsigned tries = 0; // prevent infinite loop if we don't find a space
			do
			{
				bool found = false;
				for (unsigned i = lastSpace + wLen; i < c1.size() && !found; i--)
				{
					if (c1[i] == ' ' || c1[i] == '\\') // we check for a manual break here to set width correctly
					{
						c1[i] = '\\';
						found = true;
						curWid -= i - lastSpace;
						lastSpace = i;
					}
				}
				tries++;
			}
			while (curWid > wLen && tries < c1.size() / wLen + 1);
			px = p.x;
			py = p.y;
			for (unsigned int i = inverted ? 1 : 0; i < c1.length(); i++)
			{
				if (c1[i] == '\\')
				{
					px = p.x;
					py += getBrushWidth() * (FONTHEIGHT + 2);
				}
				else
				{
					setColor(bR, bG, bB);
					drawCharacter(px, py, '|');
					setColor(tR, tG, tB);
					drawCharacter(px, py, c1[i]);
					px += getBrushWidth() * (FONTWIDTH + 2);
				}
			}
			mouseMove(p.x, p.y);
		}
		else if (input == "4") // Matrix
		{
			cout << "Width: ";
			cin >> s1;
			cout << "Height: ";
			cin >> s2;
			cout << "Drawing matrix at current mouse position in 2.5 seconds...\n";
			Sleep(2500);
			GetCursorPos(&p);
			for (int cx = 0; cx < s1; cx++)
			{
				for (int cy = 0; cy < s2; cy++)
				{
					drawPoint(p.x + (cx * getBrushWidth()), p.y + (cy * getBrushWidth()));
				}
			}
			mouseMove(p.x, p.y);
		}
		else if (input == "5")
		{
			cout << "Filename: ";
			getline(cin, c1, '\n');
			cout << "Drawing image at current mouse position in 2.5 seconds...\n";
			cout << "Please have the color window already visible!\n";
			Sleep(2500);
			GetCursorPos(&p);
			drawImage(p.x, p.y, c1.c_str());
			mouseMove(p.x, p.y);
		}
		else if (input == "5b")
		{
			cout << "Filename: ";
			getline(cin, c1, '\n');
			cout << "Drawing image with fast mode at current mouse position in 2.5 seconds...\n";
			cout << "Please have the color window already visible!\n";
			Sleep(2500);
			GetCursorPos(&p);
			drawImageFast(p.x, p.y, c1.c_str());
			mouseMove(p.x, p.y);
		}
		else if (input == "6") // Clear
		{
			cout << "Clearing screen in 2.5 seconds...\n";
			Sleep(2500);
			GetCursorPos(&p);
			for (int y = MINY; y <= MAXY && !isKeyDown(VK_ESCAPE); y += getBrushWidth())
			{
				drawLine(MINX, y, MAXX, y);
				Sleep(5);
			}
			mouseMove(p.x, p.y);
		}
		else if (input == "6b") // Clear Part
		{
			cout << "Middle click in top-left corner and then bottom-right corner to clear part\n";
			while (!isKeyDown(VK_MBUTTON))
				;
			while (isKeyDown(VK_MBUTTON))
				// give them time to unclick
				;
			int sX, sY, eX, eY;
			GetCursorPos(&p);
			sX = p.x;
			sY = p.y;
			while (!isKeyDown(VK_MBUTTON))
				;
			while (isKeyDown(VK_MBUTTON))
				// give them time to unclick
				;
			GetCursorPos(&p);
			eX = p.x;
			eY = p.y;
			cout << "Clearing screen from (" << sX << ", " << sY << ") to (" << eX << ", " << eY << ")...\n";
			for (int y = sY; y <= eY && !isKeyDown(VK_ESCAPE); y += getBrushWidth())
			{
				drawLine(sX, y, eX, y);
				Sleep(5);
			}
			mouseMove(sX, sY);
		}
		else if (input == "7") // Set cursor pos
		{
			cout << "X: ";
			cin >> s1;
			cout << "Y: ";
			cin >> s2;
			mouseMove(s1 + MINX, s2 + MINY);
		}
		else if (input == "8") // Change color
		{
			cout << "R (0 - 255): ";
			cin >> s1;
			cout << "G (0 - 255): ";
			cin >> s2;
			cout << "B (0 - 255): ";
			cin >> s3;
			s1 = min(max(0, s1), 255);
			s2 = min(max(0, s2), 255);
			s3 = min(max(0, s3), 255);
			cout << "Setting color in 2.5 seconds...\n";
			cout << "Please have the color window already visible!\n";
			Sleep(2500);
			GetCursorPos(&p);
			setColor(s1, s2, s3);
			mouseMove(p.x, p.y);
		}
		else if (input == "9") // Change brush width
		{
			cout << "Width: ";
			cin >> s1;
			setBrushWidth(s1);
			cout << "Brush width set to " << s1 << endl;
		}
		else if (input == "10") // Reload font
		{
			cout << "Reloading font...\n";
			loadFont();
			cout << "Font reloaded.\n\n";
		}
		else if (input == "exit")
		{
			running = false;
		}
		else if (input != "")
		{
			cout << "Invalid option \"" << input << "\"\n";
		}
	}

	return 0;
}
