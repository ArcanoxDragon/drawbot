/*
 * settings.h
 *
 *  Created on: May 28, 2013
 *      Author: Brian
 */

#ifndef SETTINGS_H_
#define SETTINGS_H_

#define MINX 80
#define MAXX 1360
#define MINY 112
#define MAXY 832
#define MIDX ((MAXX - MINX) / 2) + MINX
#define MIDY ((MAXY - MINY) / 2) + MINY
#define B_WIDTH 4
#define FAST_COLORS 16
#define DENSITY 1

#define C_MINX 14
#define C_MINY 99
#define C_MAXX 113
#define C_MAXY 198
#define C_HUEX 125

// Uncomment to enable debug output files
//#define DEBUG

#endif /* SETTINGS_H_ */
