/*
 * DrawTools.cpp
 *
 *  Created on: May 28, 2013
 *      Author: Brian
 */

#define _USE_MATH_DEFINES
//#define WINVER 0x0500
#define _WIN32_WINNT 0x0500
#include "DrawTools.h"
#include "FontLoader.h"
#include "settings.h"
#include "imageloader.h"
#include "vectorhelper.h"
#include <windows.h>
#include <cmath>
#include <math.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>
using namespace std;

int curR = -1;
int curG = -1;
int curB = -1;
int bWidth = B_WIDTH;
bool imgFast = true;

void setFastImage(bool fastImage)
{
	imgFast = fastImage;
}

bool getFastImage()
{
	return imgFast;
}

void drawPoint(int x, int y)
{
	for (int i = 0; i < DENSITY; i++)
	{
		mouseMove(x, y);
		mouseDown();
		mouseUp();
	}
}

void drawLine(int sX, int sY, int eX, int eY)
{
	mouseMove(sX, sY);
	mouseDown();
	Sleep(15);
	mouseMove(eX, eY);
	mouseUp();
}

void setColor(int r, int g, int b)
{
	int tR, tG, tB;
	tR = max(min(r, 255), 0);
	tG = max(min(g, 255), 0);
	tB = max(min(b, 255), 0);
	if (curR != tR || curG != tG || curB != tB)
	{
		double fR = (((double) tR) / 255.0);
		double fG = (((double) tG) / 255.0);
		double fB = (((double) tB) / 255.0);
		double fH, fS, fV;
		RGB2HSV(fR, fG, fB, fH, fS, fV);
		fH -= 10.0 / 360.0;
		while (fH < 0.0)
			fH += 1.0;
		while (fH >= 1.0)
			fH -= 1.0;
		int cHY = ((int) (fH * (double) (C_MAXY - C_MINY))) + C_MINY;
		int cHX = C_HUEX;
		int cX = ((int) (fS * (double) (C_MAXX - C_MINX))) + C_MINX;
		int cY = ((int) ((1 - fV) * (double) (C_MAXY - C_MINY))) + C_MINY;
		SetCursorPos(cHX, cHY); //this is the only place we can use SetCursorPos; no bounds checking here
		mouseDown();
		Sleep(1);
		mouseUp();
		SetCursorPos(cX, cY);
		mouseDown();
		Sleep(1);
		mouseUp();
		curR = tR;
		curG = tG;
		curB = tB;
	}
}

void setBrushWidth(int width)
{
	bWidth = width;
}

int getBrushWidth()
{
	return bWidth;
}

void drawCircle(int x, int y, int r)
{
	double angle = 0;
	int lX = x + r * cos(angle);
	int lY = y + r * sin(angle);
	int cX, cY;
	double del = M_PI / 16.0f;
	for (angle = 0.0; angle <= 2.0 * M_PI + del; angle += del)
	{
		cX = x + r * cos(angle);
		cY = y + r * sin(angle);
		drawLine(cX, cY, lX, lY);
		Sleep(5);
		//drawPoint(cX, cY);
		lX = cX;
		lY = cY;

		if (isKeyDown(VK_ESCAPE))
			return;
	}
}

void drawRectangle(int x, int y, int w, int h)
{
	drawLine(x, y, x + w, y);
	Sleep(5);
	drawLine(x + w, y, x + w, y + h);
	Sleep(5);
	drawLine(x + w, y + h, x, y + h);
	Sleep(5);
	drawLine(x, y + h, x, y);
	Sleep(5);
}

void drawCharacter(int x, int y, char c)
{
	bool *px = getLetter(c);
	bool cp;
	for (int cx = 0; cx < FONTWIDTH; cx++)
	{
		for (int cy = 0; cy < FONTHEIGHT; cy++)
		{
			cp = px[cy * FONTWIDTH + cx];
			if (cp)
				drawPoint(x + cx * bWidth, y + cy * bWidth);
			//drawCircle(x + cx * B_WIDTH * 2, y + cy * B_WIDTH * 2, B_WIDTH);

			if (isKeyDown(VK_ESCAPE))
				return;
		}
	}
}

void compressImage(int* img, unsigned w, unsigned h)
{
	// compress image
	for (unsigned i = 0; i < w * h; i++)
	{
		int r, g, b, a;
		r = img[i] & 0xFF;
		g = (img[i] >> 8) & 0xFF;
		b = (img[i] >> 16) & 0xFF;
		a = (img[i] >> 24) & 0xFF;
		int nr, ng, nb, na;
		int fac = 256 / FAST_COLORS;
		nr = r / fac;
		ng = g / fac;
		nb = b / fac;
		na = a;
		nr *= fac;
		ng *= fac;
		nb *= fac;
		int t = 0;
		t += nr & 0xFF;
		t += (ng & 0xFF) << 8;
		t += (nb & 0xFF) << 16;
		t += (na & 0xFF) << 24;
		img[i] = t;
	}
}

void drawImage(int x, int y, int *image, unsigned w, unsigned h, ImageOverrideType override)
{
#ifdef DEBUG
	if (override == OVERRIDE_NONE)
	{
		cout << "No override on image.\n";
	}
	else if (override == OVERRIDE_FAST)
	{
		cout << "Fast override on image.\n";
	}
	else
	{
		cout << "Sequential override on image.\n";
	}
#endif
	vector<int> *colors = new vector<int>();
	for (unsigned i = 0; i < w * h; i++)
	{
		int c = image[i] & 0xFFFFFF;
		if (!vectorContains(colors, c))
		{
			colors->push_back(c);
		}
	}
	if ((colors->size() <= 512 || override == OVERRIDE_FAST) && override != OVERRIDE_SEQU)
	{
#ifdef DEBUG
		ofstream o("i_debug.txt");
		o << "The image has " << colors->size() << " colors.\n";
#endif
		for (unsigned i = 0; i < colors->size(); i++)
		{
			int tC = colors->at(i);
			int tR = tC & 0xFF;
			int tG = (tC >> 8) & 0xFF;
			int tB = (tC >> 16) & 0xFF;
#ifdef DEBUG
			o << (i + 1) << ". (" << tR << ", " << tG << ", " << tB << ")\n";
#endif
			for (unsigned iX = 0; iX < w; iX++)
			{
				for (unsigned iY = 0; iY < h; iY++)
				{
					int c = image[iY * w + iX];
					int cR = c & 0xFF;
					int cG = (c >> 8) & 0xFF;
					int cB = (c >> 16) & 0xFF;
					int cA = (c >> 24) & 0xFF;
					if (cR == tR && cG == tG && cB == tB && cA >= 128)
					{
						setColor(tR, tG, tB);
						drawPoint(x + iX * bWidth, y + iY * bWidth);
					}
					if (isKeyDown(VK_ESCAPE))
					{
#ifdef DEBUG
						o.close();
#endif
						delete colors;
						return;
					}
				}
			}
		}
#ifdef DEBUG
		o.close();
#endif
	}
	else
	{
		for (unsigned iX = 0; iX < w; iX++)
		{
			for (unsigned iY = 0; iY < h; iY++)
			{
				int c = image[iY * w + iX];
				int cR = c & 0xFF;
				int cG = (c >> 8) & 0xFF;
				int cB = (c >> 16) & 0xFF;
				int cA = (c >> 24) & 0xFF;
				if (cA >= 128)
				{
					setColor(cR, cG, cB);
					drawPoint(x + iX * bWidth, y + iY * bWidth);
				}
				if (isKeyDown(VK_ESCAPE))
				{
					delete colors;
					return;
				}
			}
		}
	}
	delete colors;
}

void drawImage(int x, int y, const char *fname)
{
	unsigned w, h;
	if (strlen(fname) > 0)
	{
		ImageOverrideType ovrd = OVERRIDE_NONE;
		if (fname[0] == '`')
			ovrd = OVERRIDE_FAST;
		if (fname[0] == '~')
			ovrd = OVERRIDE_SEQU;
		int* img = loadImage((ovrd == OVERRIDE_NONE ? fname : fname + sizeof(char)), w, h);
		if (img != 0)
		{
			drawImage(x, y, img, w, h, ovrd);
		}
		delete img;
	}
}

void drawImageFast(int x, int y, const char* fname)
{
	unsigned w, h;
	if (strlen(fname) > 0)
	{
		ImageOverrideType ovrd = OVERRIDE_NONE;
		if (fname[0] == '`')
			ovrd = OVERRIDE_FAST;
		if (fname[0] == '~')
			ovrd = OVERRIDE_SEQU;
		int* img = loadImage((ovrd == OVERRIDE_NONE ? fname : fname + sizeof(char)), w, h);
		if (img != 0)
		{
			compressImage(img, w, h);
			drawImage(x, y, img, w, h, ovrd);
		}
		delete img;
	}
}

void mouseDown()
{
	mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, GetMessageExtraInfo());
}

void mouseUp()
{
	mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, GetMessageExtraInfo());
}

void mouseMove(int x, int y)
{
	SetCursorPos(min(max(x, MINX), MAXX), min(max(y, MINY), MAXY));
}

bool isKeyDown(int key)
{
	return (GetKeyState(key) & 0x80) != 0;
}

void RGB2HSV(double r, double g, double b, double &h, double &s, double &v)
{
	double rgb_max = max(r, max(g, b));
	double rgb_min = min(r, min(g, b));
	double delta = rgb_max - rgb_min;
	s = delta / (rgb_max + 1e-20);
	v = rgb_max;

	double hue;
	if (r == rgb_max)
		hue = (g - b) / (delta + 1e-20);
	else if (g == rgb_max)
		hue = 2 + (b - r) / (delta + 1e-20);
	else
		hue = 4 + (r - g) / (delta + 1e-20);
	if (hue < 0)
		hue += 6.0;
	h = hue * (1.0 / 6.0);
}
