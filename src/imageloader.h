/*
 * imageloader.h
 *
 *  Created on: May 29, 2013
 *      Author: Brian
 */

#ifndef IMAGELOADER_H_
#define IMAGELOADER_H_

#include <string>
using std::string;

int* loadImage(string file, unsigned &w, unsigned &h);

#endif /* IMAGELOADER_H_ */
