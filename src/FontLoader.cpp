/*
 * ASCIIConstants.cpp
 *
 *  Created on: May 28, 2013
 *      Author: Brian
 */

#include "FontLoader.h"
#include <fstream>
#include <string>
using std::ifstream;
using std::string;

bool nullChar[] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
char* chars[255];

bool* getLetter(char c)
{
	if (chars[(int) c] != 0)
	{
		bool* temp = new bool[FONTHEIGHT * FONTWIDTH];
		for (int i = 0; i < FONTHEIGHT * FONTWIDTH; i++)
		{
			temp[i] = chars[(int) c][i] == '1';
		}
		return temp;
	}
	return nullChar;
}

void loadFont()
{
	ifstream in(FONTFILE);
	string line = "";
	while (!in.eof())
	{
		getline(in, line);
		if (line.length() > 1)
		{
			if (line[0] != '`') //comment
			{
				char c = line[0];
				if (c >= 0 && c < 256)
				{
					char* cFont = new char[FONTHEIGHT * FONTWIDTH];
					int j = 0;
					for (unsigned int i = 1; i < line.length(); i++)
					{
						if (line[i] == '0' || line[i] == '1')
						{
							if (j < FONTHEIGHT * FONTWIDTH)
								cFont[j] = line[i];
							j++;
						}
						if (j < FONTHEIGHT * FONTWIDTH)
						{
							for (int k = j; k < FONTHEIGHT * FONTWIDTH; k++)
							{
								cFont[k] = '0';
							}
						}
					}
					chars[(int) c] = cFont;
				}
			}
		}
	}
	in.close();
}
