/*
 * vectorhelper.h
 *
 *  Created on: May 30, 2013
 *      Author: Brian
 */

#ifndef VECTORHELPER_H_
#define VECTORHELPER_H_

#include <vector>
using std::vector;

template<class T>
bool vectorContains(const vector<T> *vector, T value)
{
	for (unsigned i = 0; i < vector->size(); i++)
	{
		if (vector->at(i) == value)
		{
			return true;
		}
	}
	return false;
}

#endif /* VECTORHELPER_H_ */
